# jwt-spring-boot-example

Sample code for using JWT with Spring Boot Security.


## How to generate the keys

This example uses the signature algorithm RS256 for signing the tokens.
Use the following commands to generate the keys and transform them into a format `java.security` can handle.

```
openssl genrsa -out key

openssl rsa -in key -pubout -outform PEM -out public-key.pem

openssl pkcs8 -topk8 -inform PEM -in key -out private-key.pem -nocrypt
```

**Important:**

Never push the private key to a git repository!
With this key an attacker can create fake tokens.
Use Spring Cloud Configuration or Kubernetes Secrets for your implementation!


## jwt-issuer

Offers a login POST request (see `http/auth-client.http`).

On success, this service creates a JWT containing the user and the roles.
It signs the token with the private key specified in `application.yaml`.
The token expiration time can be configured via `jwt.token-validity-seconds` in `application.yaml`.

**Test users / passwords (groups):**
* alice / pass (ROLE_USER)
* bob / pass (ROLE_USER, ROLE_ADMIN)


## protected-service

Offers two GET requests `/hello` and `/hello-admin`.

For testing:

1. Login and get a token via `jwt-issuer`.
2. Add the received token to `http/http-client.env.json`.
3. Try the GET requests from `http/http-requests.http`


## Advanced topics

For a more sophisticated flow, please have a look at OAuth2 and OpenID:
* e.g. with Spring Boot: https://spring.io/guides/tutorials/spring-boot-oauth2/
