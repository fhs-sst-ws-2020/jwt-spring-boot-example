package net.schoeninger.jwtissuer.services;

import net.schoeninger.jwtissuer.domain.User;
import net.schoeninger.jwtissuer.util.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private JwtService jwtService;

    @Autowired
    public AuthenticationServiceImpl(JwtService jwtService) {
        this.jwtService = jwtService;
    }

    @Override
    public String login(String username, String password) {
        Optional<User> userOptional = checkCredentials(username, password);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            Map<String, Object> claims = getClaims(user.getRoles());
            return jwtService.generateToken(user.getUsername(), claims);
        } else {
            throw new UnauthorizedException("invalid credentials");
        }
    }

    private Optional<User> checkCredentials(String username, String password) {
        // TODO 4 Students:
        // Use a database, spring boot security's LDAP authentication,
        // or other user directory service for managing users and groups!
        // This example project only shows how to create and sign JWTs.
        User user = null;
        if (username.equals("alice") && password.equals("pass")) {
            user = User.builder().username(username).roles("ROLE_USER").build();
        } else if (username.equals("bob") && password.equals("pass")) {
            user = User.builder().username(username).roles("ROLE_USER,ROLE_ADMIN").build();
        }
        return Optional.ofNullable(user);
    }

    private Map<String, Object> getClaims(String roles) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("roles", roles);
        return claims;
    }

}
