package net.schoeninger.jwtissuer.services;

import java.util.Map;

public interface JwtService {

    String generateToken(String subject, Map<String, Object> claims);

}
