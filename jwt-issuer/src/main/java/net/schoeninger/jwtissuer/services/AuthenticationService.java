package net.schoeninger.jwtissuer.services;

public interface AuthenticationService {

    String login(String username, String password);

}
