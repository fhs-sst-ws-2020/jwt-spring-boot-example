package net.schoeninger.jwtissuer.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@NoArgsConstructor @AllArgsConstructor @Getter
public class Credentials {

    @NotEmpty
    private String username;

    @NotEmpty
    private String password;

}
