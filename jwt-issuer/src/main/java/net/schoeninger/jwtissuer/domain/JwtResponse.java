package net.schoeninger.jwtissuer.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor @AllArgsConstructor @Getter
public class JwtResponse {

    private String jwt;

}
