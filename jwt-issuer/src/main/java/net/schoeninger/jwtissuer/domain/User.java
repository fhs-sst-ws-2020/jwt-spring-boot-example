package net.schoeninger.jwtissuer.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@AllArgsConstructor @Getter @Builder
public class User {

    private String username;
    private String roles;

}
