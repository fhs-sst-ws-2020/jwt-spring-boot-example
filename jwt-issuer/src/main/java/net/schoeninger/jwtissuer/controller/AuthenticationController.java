package net.schoeninger.jwtissuer.controller;

import net.schoeninger.jwtissuer.domain.Credentials;
import net.schoeninger.jwtissuer.domain.JwtResponse;
import net.schoeninger.jwtissuer.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthenticationController {

    private AuthenticationService authenticationService;

    @Autowired
    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping("/login")
    public JwtResponse login(@RequestBody @Valid Credentials credentials) {
        String jwt = authenticationService.login(credentials.getUsername(), credentials.getPassword());
        return new JwtResponse(jwt);
    }

}
