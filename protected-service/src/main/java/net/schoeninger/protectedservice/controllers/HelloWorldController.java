package net.schoeninger.protectedservice.controllers;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class HelloWorldController {

    @GetMapping("/hello")
    @Secured("ROLE_USER")
    public String helloWorld() {
        String user = getAuthenticatedUser();
        return "Hello " + user + "!";
    }

    @GetMapping("/hello-admin")
    @Secured("ROLE_ADMIN")
    public String helloAdmin() {
        String user = getAuthenticatedUser();
        return "Hello admin " + user + "!";
    }

    private String getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            return authentication.getName();
        } else {
            return null;
        }
    }

}
