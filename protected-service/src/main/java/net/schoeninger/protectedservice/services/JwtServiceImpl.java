package net.schoeninger.protectedservice.services;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

@Service
public class JwtServiceImpl implements JwtService {

    @Value("${jwt.public-key}")
    private String publicKey;

    @Override
    @SneakyThrows
    public Jws<Claims> parseAndValidate(String jwt) {
        PublicKey publicKey = getPublicKey();
        return Jwts.parser().setSigningKey(publicKey).parseClaimsJws(jwt);
    }

    private PublicKey getPublicKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
        String preProcessedKey = publicKey.replaceAll("\n", "");
        byte[] key = Base64.getDecoder().decode(preProcessedKey);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(key);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePublic(keySpec);
    }

}
